<?php
require_once('messages_api.php');
if(!isset($_SESSION['myusername'])){
    $result = getAllMessages();
} else if (isset($_POST['_submit'])) {
    $title = $_POST['message_title'];
    $text = $_POST['message_text'];
    if (!checkMessageExists($title, $text))
        setMessageForUser($title, $text);
    $result = getMessagesForUser();
} else {
    $result = getMessagesForUser();
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Hello World</title>
    <link rel="stylesheet" type="text/css" href="messages.css" /> 
    <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
    window.setInterval(function(){
        $.ajax({
            url: 'messages_ajax.php?tstamp=' + new Date().toISOString(),
            dataType: 'json',
            success: function(data) {
                for (var i=0; i<data.length; i++) {
                    var message = data[i];
                    var div = $('<div class="message_block"></div>');
                    var title = $('<div class="title"></div>');
                    title.text('The title is '+message.title);
                    var msg = $('<div class="message"></div>');
                    msg.text('The message is '+message.text);
                    var time = $('<div class="time"></div>');
                    time.text('The message was written at '+message.time);
                    var user = $('<div class="user"></div>');
                    user.text('The user who wrote it is: '+message.username);
                    var image = $('<div class="image"></div>');
                    image.text('Here is the user image: ');
                    var img = $('<img src="'+message.image_url+'">');
                    image.append(img);
                    div.append(title);
                    div.append(message);
                    div.append(time);
                    div.append(user);
                    div.append(image);
                    div.append($('<hr>'));
                    div.insertBefore('#messages:first-child');
                }
            }
        });
    }, 20000);
    </script>
</head>
<body>
<div id="messages">
<?php foreach ($result->fetchAll() as $info): ?>
<div class="message_block">
    <div class="title">The title is  <?php echo $info['title']; ?></div>
    <div class="message">The message is <?php echo $info['text']; ?></div>
    <div class="time">The message was written at <?php echo $info['time']; ?></div>
    <div class="user">The user who wrote it is <?php echo $info['username']; ?></div>
    <div class="image"><img src="<?php echo $info['image_url']; ?>"></div>
</div>
<hr>
<?php endforeach; ?>
</div>

<?php if (isset($_SESSION['myusername'])): ?>
<!-- Here is the form code -->

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<label for="message_title">Title: </label>
  <input id="message_title" type="text" name="message_title">
<label for="message_text">Text: </label>
  <input id="message_text" type="text" name="message_text">
<input type="submit" name="_submit">
</form>
<?php else: ?>
<div style="position:fixed;top:3px;left:0;width:98%;text-align:right;">
    <a href="login.php" title="Log in to Twatter">Log in</a>
</div>
<?php endif; ?>
</body>
</html>
