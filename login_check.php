<?php
require_once('login_api.php');

// original code from: http://www.phpeasystep.com/workshopview.php?id=6
// modified by Bob Bradley 9/5/2014 to work with php 5

ob_start();
session_start();

// Define $myusername and $mypassword 
$myusername=$_POST['myusername']; 
$mypassword=$_POST['mypassword']; 

// To protect MySQL injection (more detail about MySQL injection)
$result = getUserInfo($myusername,$mypassword);

// Mysql_num_row is counting table row
$count=$result->rowCount();

// If result matched $myusername and $mypassword, table row must be 1 row
if($count==1){

// Register $myusername, $mypassword and redirect to file "login_success.php"
//session_register("myusername"); // does not work in php5
$_SESSION['myusername']=$myusername;
//session_register("mypassword"); 
$_SESSION['mypassword']=$mypassword;
header("location:messages.php");
}
else {

echo "Wrong Username or Password";
// Clear the session variables if it is a bad login
$_SESSION = array();

}
ob_end_flush();
