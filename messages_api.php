<?php
require_once('login_api.php');
$result = getUserInfo($_SESSION['myusername'], $_SESSION['mypassword']);
if ($result->rowCount()) {
    $user_id = $result->fetchAll()[0][0];
}


function checkMessageExists($title, $text){
    global $dbh, $user_id;
    $sql = <<<SQL
SELECT message.id, message.title, message.time, user.username, user.image_url
FROM Messages AS message, members AS user
  WHERE message.user = :user_id
  AND message.title = :title
  AND message.text = :text;
SQL;
    $stmt = $dbh->prepare($sql);
    $stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':text', $text);
    $stmt->execute();
    return boolval($stmt->rowCount());
}


function deletePostById($id){
    global $dbh;
    $sql = 'DELETE FROM messages WHERE id=:id';
    $stmt = $dbh->prepare($sql);
    $stmt->bindParam(':id', $id);
    return $stmt->execute();
}


function getAllMessages(){
    global $dbh;
    $sql = <<<SQL
SELECT message.id, message.title, message.text, message.time, user.username, user.image_url
FROM Messages AS message, members AS user
  WHERE message.user = user.id
ORDER BY message.time DESC;
SQL;
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
    return $stmt;
}


function getAllMessagesSince($tstamp){
    global $dbh;
    $sql = <<<SQL
SELECT message.id, message.title, message.text, message.time, user.username, user.image_url
FROM Messages AS message, members AS user
  WHERE message.user = user.id
  AND message.time > :tstamp
ORDER BY message.time DESC;
SQL;
    $time = strtotime($tstamp);
    $stmt = $dbh->prepare($sql);
    $stmt->bindParam(':tstamp', date('Y-m-d h:i:s', $time));
    $stmt->execute();
    return $stmt;
}


function getMessagesForUser(){
    global $user_id;
    if ($user_id) {

        global $dbh;
        $sql = <<<SQL
SELECT message.id, message.title, message.text, message.time, message.latitude, message.longitude,
       user.username, user.image_url
FROM Messages AS message, members AS user
  WHERE message.user = :user_id
  AND user.id = :user_id
ORDER BY message.time DESC;
SQL;
        $stmt=$dbh->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        
        $stmt->execute();
        return $stmt;
    }
    return null;
}


function setMessageForUser($message_title, $message_text, $message_location){
    global $user_id;
    if ($user_id) {
        global $dbh;
        $sql = <<<SQL
INSERT INTO Messages (title, text, user, latitude, longitude)
VALUES (:title, :text, :user_id, :latitude, :longitude);
SQL;
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':title', $message_title);
        $stmt->bindParam(':text', $message_text);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':latitude', $message_location[0]);
        $stmt->bindParam(':longitude', $message_location[1]);

        $stmt->execute();
        return $stmt->rowCount();
    }
    return 0;
}
