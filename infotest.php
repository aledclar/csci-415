<?php
require_once('messages_api.php');
if(!isset($_SESSION['myusername'])){
    $result = getAllMessages();
} else if (isset($_POST['_submit'])) {
    $title = $_POST['message_title'];
    $text = $_POST['message_text'];
    $location = array(
        $_POST['message_latitude'],
        $_POST['message_longitude']
    );
    if (!checkMessageExists($title, $text))
        setMessageForUser($title, $text, $location);
    $result = getMessagesForUser();
} else {
    $result = getMessagesForUser();
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Welcome to Twatter</title>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="messagestest.css" /> 
    <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="js/libs/modernizr.js"></script>
    <script>
    window.setInterval(function(){
        $.ajax({
            url: 'messages_ajax.php?tstamp=' + new Date().toISOString(),
            dataType: 'json',
            success: function(data) {
                for (var i=0; i<data.length; i++) {
                    var message = data[i];
                    var div = $('<div class="message_block"></div>');
                    var title = $('<div class="title"></div>');
                    title.text('The title is '+message.title);
                    var msg = $('<div class="message"></div>');
                    msg.text('The message is '+message.text);
                    var time = $('<div class="time"></div>');
                    time.text('The message was written at '+message.time);
                    var user = $('<div class="user"></div>');
                    user.text('The user who wrote it is: '+message.username);
                    var image = $('<div class="image"></div>');
                    image.text('Here is the user image: ');
                    var img = $('<img src="'+message.image_url+'">');
                    image.append(img);
                    div.append(title);
                    div.append(msg);
                    div.append(time);
                    div.append(user);
                    div.append(image);
                    div.append($('<hr>'));
                    div.insertBefore('#messages:first-child');
                }
            }
        });
    }, 20000);
    function set_location(pos) {
        document.getElementById('message_latitude').value = pos.coords.latitude;
        document.getElementById('message_longitude').value = pos.coords.longitude;
    	
	}
    function set_location_fail(pos_error) {
        alert(pos_error.message);
    }
    function get_location() {
        if (Modernizr.geolocation) {
            navigator.geolocation.getCurrentPosition(set_location, set_location_fail);
        } else {
            alert('This browser does not support geolocation...');
        }
    }

    function initialize_map(pos, canvas, text) {
        var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
        var gmap_settings = {
            zoom: 15,
            center: latlng,
            draggable: false,
            keyboardShortcuts: false,
            mapTypeControl: false,
            navigationControl: false,
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            scrollwheel: false,
			disableDefaultUI: true
        
		};
		console.log("latitude: " + pos.coords.latitude);
         var infowindow = new google.maps.InfoWindow({
				       content:  "Latitude: " + pos.coords.latitude +  "<br/> Longitude: " + pos.coords.longitude
					   
					     });
		var map = new google.maps.Map(canvas, gmap_settings);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: text,
});
     google.maps.event.addListener(marker, 'click', function() {
			     infowindow.open(map,marker);
				  if (marker.getAnimation() != null) {
						      marker.setAnimation(null);
							    }
				else{
			marker.setAnimation(google.maps.Animation.BOUNCE);
}
}); 
	}
	

$(document).ready(function(){
        $('#message_add_location').change(function(){ 
            if ($(this).is(':checked')) get_location();
        });
        $('.map_canvas').each(function(){
            var self = $(this);
            var pos = {coords: {
                latitude: self.data('lat'),
                longitude: self.data('lng')
            }};
            var text = self.data('txt');
            initialize_map(pos, this, text);
        });
    });
    </script>
    <style>.map_canvas{width:250px;height:200px;margin:0 auto;}</style>
</head>
<body>
<div id="map_canvas"></div>
<div id="messages">
<?php foreach ($result->fetchAll() as $info): ?>
<div class="message_block">
    <div class="title">Title: <?php echo $info['title']; ?></div>
    <div class="message">Message: <?php echo $info['text']; ?></div>
    <div class="time">Time: <?php echo $info['time']; ?></div>
    <div class="user">User: <?php echo $info['username']; ?></div>
 	<div class="image"><img src="<?php echo $info['image_url']; ?>"></div>
	<br>
<!--	<div class="locationOutput">Your Location is Latitude: <?php echo $info['latitude']; ?> and Longitude: <?php echo $info['longitude']; ?> </div>
	
!-->
	<?php if ($info['latitude'] && $info['longitude']): ?>
    <div class="map_canvas" data-lat="<?php echo $info['latitude'];?>"
                            data-lng="<?php echo $info['longitude'];?>"
                            data-txt="<?php echo $info['text'];?>">
 	
 	</div>
    <?php endif; ?>
</div>
<hr>
<?php endforeach; ?>
</div>

<?php if (isset($_SESSION['myusername'])): ?>
<!-- Here is the form code -->

<form id="new_message_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<label for="message_title">Title: </label>
  <input id="message_title" type="text" name="message_title">
<label for="message_text">Text: </label>
  <input id="message_text" type="text" name="message_text">
  <input id="message_latitude" type="hidden" name="message_latitude">
  <input id="message_longitude" type="hidden" name="message_longitude">
  <input id="message_add_location" type="checkbox" value="true">
  <label for="message_add_location">Save my Location</label>
<input type="submit" name="_submit">
</form>
<?php else: ?>
<div style="position:fixed;top:3px;left:0;width:98%;text-align:right;">
    <a href="login.php" title="Log in to Twatter">Log in</a>
</div>
<?php endif; ?>
</body>
</html>
