<?php
if (!isset($_GET['tstamp'])) {
    die('No timestamp given.');
} else {
    require_once('messages_api.php');
    $result = getAllMessagesSince($_GET['tstamp']);
    if (!$result->rowCount()) {
        echo '';
    } else {
        echo json_encode($result->fetchAll());
    }
}
